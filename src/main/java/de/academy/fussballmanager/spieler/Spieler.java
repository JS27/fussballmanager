package de.academy.fussballmanager.spieler;

import de.academy.fussballmanager.verein.Verein;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Spieler {
    @Id
    @GeneratedValue
    private int spielerId;

    private String spielerName;

    @ManyToOne
    private Verein verein;

    public Spieler() {
    }

    public int getSpielerId() {
        return spielerId;
    }

    public void setSpielerId(int spielerId) {
        this.spielerId = spielerId;
    }

    public String getSpielerName() {
        return spielerName;
    }

    public void setSpielerName(String spielerName) {
        this.spielerName = spielerName;
    }

    public Verein getVerein() {
        return verein;
    }

    public void setVerein(Verein verein) {
        this.verein = verein;
    }
}
