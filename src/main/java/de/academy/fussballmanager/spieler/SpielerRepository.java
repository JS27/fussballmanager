package de.academy.fussballmanager.spieler;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SpielerRepository extends CrudRepository<Spieler, Integer> {
    List<Spieler> findAll();
}
