package de.academy.fussballmanager.spieler;

import de.academy.fussballmanager.verein.Verein;
import de.academy.fussballmanager.verein.VereinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class SpielerController {
    private SpielerService spielerService;
    private SpielerRepository spielerRepository;
    private VereinService vereinService;


    @Autowired
    public SpielerController(SpielerService spielerService, VereinService vereinService){
        this.spielerService = spielerService;
        this.vereinService = vereinService;
    }

    @GetMapping("/fb-manager/{vereinId}")
    public String entryFormSpieler(Model model, @PathVariable int vereinId){
        Verein aktVerein = vereinService.getVerein(vereinId);
        Spieler spieler = new Spieler();
        model.addAttribute("spieler", spieler);
        model.addAttribute("verein", aktVerein);
        model.addAttribute("mannschaft", spielerService.getSpielerListe(vereinId));
        return "tabellenAusgabeMannschaft";
    }

    @PostMapping("/fb-manager/{vereinId}")
    public String entrySubmitSpieler(Model model, @ModelAttribute Spieler spieler, @Valid BindingResult bindingResult, @PathVariable int vereinId){
        if(bindingResult.hasErrors()){
            return "tabellenAusgabeMannschaft";
        }
        Verein aktVerein = vereinService.getVerein(vereinId);
        model.addAttribute(aktVerein);
            spieler.setVerein(aktVerein);
            spielerService.addSpieler(spieler);
            spielerService.removeSpieler(spieler);
        return entryFormSpieler(model, vereinId);
    }
}
