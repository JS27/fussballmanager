package de.academy.fussballmanager.spieler;

import de.academy.fussballmanager.verein.VereinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpielerService {

    private SpielerRepository spielerRepository;
    private VereinRepository vereinRepository;

    @Autowired
    public SpielerService(SpielerRepository spielerRepository, VereinRepository vereinRepository){
        this.spielerRepository =spielerRepository;
        this.vereinRepository = vereinRepository;
    }

    public List<Spieler> getSpielerListe(int vereinId){
    return ((vereinRepository.findById(vereinId)).get()).getMannschaft();
    }

    public void addSpieler(Spieler spieler){

        spielerRepository.save(spieler);
    }

    public void removeSpieler(Spieler spieler){

        spielerRepository.delete(spieler);
    }

}
