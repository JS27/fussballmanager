package de.academy.fussballmanager.verein;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class VereinController {
    private VereinService vereinService;
    private VereinRepository vereinRepository;

    @Autowired
    public VereinController(VereinService vereinService) {

        this.vereinService = vereinService;
    }

    @GetMapping("/fb-manager")
    public String entryForm(Model model){
        Verein verein = new Verein();
        model.addAttribute("verein", verein);
        model.addAttribute("vereinListe", vereinService.getVereinliste());
        return "tabellenAusgabeVerein";
    }

    @PostMapping("/fb-manager")
    public String entrySubmit(Model model, @ModelAttribute Verein verein, @Valid BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "tabellenAusgabeVerein";
        }
        vereinService.addVerein(verein);
        return entryForm(model);
    }

}
