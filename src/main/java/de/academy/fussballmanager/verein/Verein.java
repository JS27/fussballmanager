package de.academy.fussballmanager.verein;

import de.academy.fussballmanager.spieler.Spieler;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Verein {
    @Id
    @GeneratedValue
    private int vereinId;

    private String vereinName;

    @OneToMany (mappedBy = "verein")
    private List<Spieler> mannschaft;

    public Verein() {
    }

    public int getVereinId() {
        return vereinId;
    }

    public void setVereinId(int vereinId) {
        this.vereinId = vereinId;
    }

    public String getVereinName() {
        return vereinName;
    }

    public void setVereinName(String vereinName) {
        this.vereinName = vereinName;
    }

    public List<Spieler> getMannschaft() {
        return mannschaft;
    }

    public void setMannschaft(List<Spieler> mannschaft) {
        this.mannschaft = mannschaft;
    }
}
