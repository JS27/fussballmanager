package de.academy.fussballmanager.verein;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VereinService {

    private VereinRepository vereinRepository;


    @Autowired
    public VereinService(VereinRepository vereinRepository) {
        this.vereinRepository = vereinRepository;
    }

    public List<Verein> getVereinliste(){

        return vereinRepository.findAll();
    }

    public void addVerein(Verein verein){
        vereinRepository.save(verein);
    }

    public Verein getVerein(int vereinId){
        return vereinRepository.findById(vereinId).get();
    }


}
