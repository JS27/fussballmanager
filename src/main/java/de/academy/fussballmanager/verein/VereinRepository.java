package de.academy.fussballmanager.verein;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface VereinRepository extends CrudRepository<Verein, Integer> {
    List<Verein> findAll();
//    List<Verein> findByVereinId(int vereinId);
}
